<?php
return array(
    'URL_MODEL'=>0, // 如果你的环境不支持PATHINFO 请设置为3,

    /* 数据库配置 */
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => 'localhost', // 服务器地址
    'DB_NAME'   => 'xiaowei', // 数据库名
    'DB_USER'   => 'xiaowei', // 用户名
    'DB_PWD'    => 'xiaowei',  // 密码
    'DB_PORT'   => '3306', // 端口
    'DB_PREFIX' => 'think_', // 数据库表前缀
    );
